FROM nginx:alpine

LABEL maintenainer="Pierre Pottié <pierre.pottie@gmail.com>"

VOLUME /var/log/nginx

COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./conf.d/default.conf /etc/nginx/conf.d/default.conf
